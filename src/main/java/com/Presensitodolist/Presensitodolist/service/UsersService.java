package com.Presensitodolist.Presensitodolist.service;

import com.Presensitodolist.Presensitodolist.dto.LoginDto;
import com.Presensitodolist.Presensitodolist.dto.UsersDto;
import com.Presensitodolist.Presensitodolist.model.Users;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface UsersService {
  // setelah membuat repository, membuat package Service berisi method yang saling terhubung dengan userServiceImpl

    Map<String, Object> login (LoginDto loginDto);

    List<Users> getAllUsers();

    Users addUsers(UsersDto usersDto);

    Users getUsers(Integer Id);

    void deleteUsersById(Integer Id);

    @Transactional
    Users editUsers(Integer Id, Users users, MultipartFile multipartFile);

}
