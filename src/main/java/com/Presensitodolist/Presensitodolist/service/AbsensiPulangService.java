package com.Presensitodolist.Presensitodolist.service;

import com.Presensitodolist.Presensitodolist.dto.AbsensiPulangDto;
import com.Presensitodolist.Presensitodolist.model.AbsensiPulang;

import java.util.List;

public interface AbsensiPulangService {
    // setelah membuat Repository, buat AbsensiPulangService
    // lalu buat AbsensiPulangServiceImpl

    public List<AbsensiPulang> getAll();

    AbsensiPulang getAbsensi(Integer Id);

    AbsensiPulang addAbsensi(AbsensiPulangDto absensiPulangDto);

    AbsensiPulang editAbsensi(Integer Id, AbsensiPulangDto absensiPulangDto);

    void deleteAbsensi(Integer Id);

    List<AbsensiPulang> getAllByUser(Integer usersId);
}
