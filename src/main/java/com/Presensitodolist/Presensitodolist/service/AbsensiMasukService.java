package com.Presensitodolist.Presensitodolist.service;

import com.Presensitodolist.Presensitodolist.dto.AbsensiMasukDto;
import com.Presensitodolist.Presensitodolist.model.AbsensiMasuk;

import java.util.List;

public interface AbsensiMasukService {
    // setelah membuat Repository, buat Service yang terhubung dengan serviceImpl
    // lalu buat AbsenMasukServiceImpl

    public List<AbsensiMasuk> getAll();

    List<AbsensiMasuk> getAllByUser(Integer userId);

    AbsensiMasuk getAbsensi(Integer Id);

    AbsensiMasuk addAbsensi(AbsensiMasukDto absensiMasukDto);

    AbsensiMasuk editAbsensi(Integer Id, AbsensiMasukDto absensiMasukDto);

    void deleteAbsensi(Integer Id);
}
