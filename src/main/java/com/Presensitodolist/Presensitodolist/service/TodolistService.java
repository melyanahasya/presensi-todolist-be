package com.Presensitodolist.Presensitodolist.service;

import com.Presensitodolist.Presensitodolist.dto.TodolistDto;
import com.Presensitodolist.Presensitodolist.model.Todolist;

import java.util.List;

public interface TodolistService {
    // setelah membuat repository, buat service
    // berisi method yang saling terhubung dengan TodolistServiceImpl
    // lalu buat TodolistServiceImpl

    List<Todolist> getAllTodolist(Integer useresId);

    Todolist addTodolist(TodolistDto todolistDto);

    Todolist getTodolist(Integer Id);

    Todolist editTodolist(Integer Id, TodolistDto todolistDto);

    void deleteTodolistById(Integer Id);

    List<Todolist> getAllByUser(Integer useresId);

    Todolist checklist(Integer Id);
}
