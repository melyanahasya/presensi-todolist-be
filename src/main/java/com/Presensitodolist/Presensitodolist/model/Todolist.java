package com.Presensitodolist.Presensitodolist.model;

import javax.persistence.*;

@Entity
@Table(name = "todolist")
public class Todolist {
    // langkah ke 11 , setelah pembuatan BE users selesai
    // buat model Todolist dan juga buat relasi dengan users
    // lalu buat TodolistDTO dan repository
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(name = "note")
    private String note;

    @Column(name = "done")
    private String done;

    @ManyToOne
    @JoinColumn(name = "useres_id")
    private Users useresId;

    public Todolist() {
    }

    public Todolist(String note, Users userId) {
        this.note = note;
        this.useresId = userId;
    }


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Users getUserId() {
        return useresId;
    }

    public void setUserId(Users userId) {
        this.useresId = userId;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }
}
