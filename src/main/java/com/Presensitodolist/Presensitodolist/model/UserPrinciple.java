package com.Presensitodolist.Presensitodolist.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class UserPrinciple implements UserDetails {
    // setelah selesai membuat controller, selanjutnya buat UserPrinciple
    private String email;

    private String password;

    private Collection<? extends GrantedAuthority> authority;

    public UserPrinciple(String email, String password, Collection<? extends GrantedAuthority> authority) {
        this.email = email;
        this.password = password;
        this.authority = authority;
    }


    public static UserPrinciple build(Users users) {
        var role = Collections.singletonList(new SimpleGrantedAuthority(users.getRole().name()));
        return new UserPrinciple(
                users.getEmail(),
                users.getPassword(),
                role
        );
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
