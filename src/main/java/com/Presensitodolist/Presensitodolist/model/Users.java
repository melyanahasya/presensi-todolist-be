package com.Presensitodolist.Presensitodolist.model;

import com.Presensitodolist.Presensitodolist.enumed.UserType;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class Users {

// langkah ke 10 membuat model login & register terlebih dahulu, setelah ini membuat usersDto dan Repository
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private  Integer Id;
   @Column(name = "email")
    private String email;
   @Column(name = "password")
    private String password;

   @Column(name = "nama")
   private String nama;

   @Lob
   @Column(name = "gambar")
   private String gambar;

   @Column(name = "telepon")
   private Long telepon;

   @Column(name = "alamat")
   private String alamat;

   @Enumerated(EnumType.STRING)
   @Column(name = "user_type")
    private UserType role;



    public Users() {
    }

    public Users(String email, String password, String nama, UserType role) {
        this.email = email;
        this.password = password;
        this.nama = nama;
        this.role = role;
    }


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getRole() {
        return role;
    }

    public void setRole(UserType role) {
        this.role = role;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public Long getTelepon() {
        return telepon;
    }

    public void setTelepon(Long telepon) {
        this.telepon = telepon;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
