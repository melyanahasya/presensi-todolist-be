package com.Presensitodolist.Presensitodolist.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "absensi_masuk")
public class AbsensiMasuk {
    // langkah ke 12, setelah BE todolist selesai buat model presensi masuk
    // setelah model akan membuat AbsenMasukDTO & AbsensiMasukRepository

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @CreationTimestamp
    @Column(name = "tanggal",updatable = false)
    private Date tanggal;

    @JsonFormat(pattern = "HH:mm:ss")
    @CreationTimestamp
    @Column(name = "absen_masuk",updatable = false)
    private Date absen_masuk;

    @Column(name = "status")
    private String status;

    @ManyToOne
    @JoinColumn(name = "users_id")
    private Users userId;

    public AbsensiMasuk() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getAbsen_masuk() {
        return absen_masuk;
    }

    public void setAbsen_masuk(Date absen_masuk) {
        this.absen_masuk = absen_masuk;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Absensi{" +
                "Id=" + Id +
                ", tanggal='" + tanggal +'\'' +
                ", absen_masuk='" + absen_masuk +'\'' +
                ", userId='" + userId +'\'' +
                ", status='" + status +'\'' +
                '}';
    }


}
