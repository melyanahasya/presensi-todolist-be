package com.Presensitodolist.Presensitodolist.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "absensi_pulang")
public class AbsensiPulang {
    // langkah ke 13, setelah BE AbsenMasuk selesai
    // buat model AbsenPulang
    // lalu buat AbsenPulangDTO & AbsenPulangRepository

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @CreationTimestamp
    @Column(name = "tanggal",updatable = false)
    private Date tanggal;

    @JsonFormat(pattern = "HH:mm:ss")
    @CreationTimestamp
    @Column(name = "absen_pulang",updatable = false)
    private Date absen_pulang;


    @Column(name = "status")
    private String status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users usersId;

    public AbsensiPulang() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getAbsen_pulang() {
        return absen_pulang;
    }

    public void setAbsen_pulang(Date absen_pulang) {
        this.absen_pulang = absen_pulang;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Users getUserId() {
        return usersId;
    }

    public void setUserId(Users userId) {
        this.usersId = userId;
    }

    @Override
    public String toString() {
        return "Absensi{" +
                "Id=" + Id +
                ", tanggal='" + tanggal +'\'' +
                ", absen_pulang='" + absen_pulang +'\'' +
                ", status='" + status +'\'' +
                ", userId='" + usersId +'\'' +
                '}';
    }

}
