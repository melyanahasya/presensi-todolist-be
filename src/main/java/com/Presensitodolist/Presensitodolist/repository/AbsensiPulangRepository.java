package com.Presensitodolist.Presensitodolist.repository;

import com.Presensitodolist.Presensitodolist.model.AbsensiPulang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AbsensiPulangRepository extends JpaRepository <AbsensiPulang, Integer>{
    // setelah membuat model & DTO, buat Repository
    // query dibawah untuk mencari field yang sudah direlasi
    // lalu buat Service

    @Query(value = "SELECT * FROM absensi_pulang WHERE user_id = ?1", nativeQuery = true)
    List<AbsensiPulang> findUser(Integer usersId);
}
