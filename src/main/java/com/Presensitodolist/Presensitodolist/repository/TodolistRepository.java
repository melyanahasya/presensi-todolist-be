package com.Presensitodolist.Presensitodolist.repository;

import com.Presensitodolist.Presensitodolist.model.Todolist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodolistRepository extends JpaRepository <Todolist, Integer>{
    // setelah membuat model dan todolistDTO, buat repository
    // query dibawah untuk mencari field yang sudah direlasi
    // lalu buat Service
    @Query(value = "SELECT * FROM todolist WHERE useres_id = ?1", nativeQuery = true)
    List<Todolist> findUser(Integer useresId);
}
