package com.Presensitodolist.Presensitodolist.repository;

import com.Presensitodolist.Presensitodolist.model.AbsensiMasuk;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AbsensiMasukRepository extends JpaRepository<AbsensiMasuk, Integer> {
    // query dibawah untuk mencari field yang sudah direlasi
    // lalu buat Service

    @Query(value = "SELECT * FROM absensi_masuk WHERE users_id = ?1", nativeQuery = true)
    List<AbsensiMasuk> findUser(Integer userId);
}
