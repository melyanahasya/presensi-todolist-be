package com.Presensitodolist.Presensitodolist.repository;

import com.Presensitodolist.Presensitodolist.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer>{
    // setelah model dan usersDTO meneruskan dengan membuat repository
    // setelah ini membuat UsersService
    Optional<Users> findByEmail(String username);
}
