package com.Presensitodolist.Presensitodolist.dto;

public class LoginDto {
    // loginDTO dibuat sebelum membuat UsersController

    private  String email;

    private  String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
