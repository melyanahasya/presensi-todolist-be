package com.Presensitodolist.Presensitodolist.dto;

public class TodolistDto {
    // Todolist DTO dibuat setelah model

    private String note;

    private Integer userId;

    public TodolistDto() {
    }

    public TodolistDto(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
