package com.Presensitodolist.Presensitodolist.dto;


public class AbsensiPulangDto {
    // dibuat setelah modal AbsenPulang

    private String status;

    private Integer userId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
