package com.Presensitodolist.Presensitodolist.controller;

import com.Presensitodolist.Presensitodolist.dto.TodolistDto;
import com.Presensitodolist.Presensitodolist.model.Todolist;
import com.Presensitodolist.Presensitodolist.respon.CommonResponse;
import com.Presensitodolist.Presensitodolist.respon.ResponHelper;
import com.Presensitodolist.Presensitodolist.service.TodolistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// setelah membuat ServiceImpl, buat TodolistController
// dan menambahkan 2 anotasi dibawah ini untuk melakukan integrasi
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/todolist")
public class TodolistController {
    @Autowired
    private TodolistService todolistService;

    @PostMapping("/post")
    public CommonResponse<Todolist> addTodolist(TodolistDto todolistDto){
        return ResponHelper.ok(todolistService.addTodolist(todolistDto));
    }

    @PutMapping("/{Id}")
    public Todolist getTodolish(@PathVariable("Id") Integer Id, @RequestBody TodolistDto todolistDto){
        return todolistService.editTodolist(Id, todolistDto);
    }

    @DeleteMapping("{Id}")
    public void deleteTodolistById(@PathVariable("Id") Integer Id){
        todolistService.deleteTodolistById(Id);
    }

    @GetMapping("all")
    public List<Todolist> getAllTodolist(Integer userId){
        return todolistService.getAllTodolist(userId);
    }

    @GetMapping("/{Id}")
    public Todolist getUsers(@PathVariable("Id") Integer Id){
        return todolistService.getTodolist(Id);
    }

    @GetMapping
    public CommonResponse<List<Todolist>> getAllByUser(@RequestParam Integer useresId){
        return ResponHelper.ok(todolistService.getAllByUser(useresId));
    }

    @PutMapping("/done/{Id}")
    public CommonResponse<Todolist> checklist(@PathVariable("Id")Integer Id ){
        return ResponHelper.ok(todolistService.checklist(Id));
    }

}
