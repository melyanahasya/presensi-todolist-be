package com.Presensitodolist.Presensitodolist.controller;

import com.Presensitodolist.Presensitodolist.dto.LoginDto;
import com.Presensitodolist.Presensitodolist.dto.UsersDto;
import com.Presensitodolist.Presensitodolist.model.Users;
import com.Presensitodolist.Presensitodolist.respon.CommonResponse;
import com.Presensitodolist.Presensitodolist.respon.ResponHelper;
import com.Presensitodolist.Presensitodolist.service.UsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


// setelah membuat usersServiceImpl,
// membuat LoginDTO untuk sign-in nantinya
// membuat controller disertai dengan anotasi untuk integrasi

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/users")
public class UsersController {
    @Autowired
    private UsersService usersService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String,Object>> login(@RequestBody LoginDto loginDto) {
        return ResponHelper.ok(usersService.login(loginDto));
    }

    @PostMapping( "/sign-up")
    public CommonResponse<Users> addUsers(UsersDto usersDto){
        return ResponHelper.ok(usersService.addUsers(usersDto));
    }

    @DeleteMapping("/{Id}")
    public void deleteUsersById(@PathVariable("Id") Integer Id) {
        usersService.deleteUsersById(Id);
    }

    @GetMapping("/all")
    public CommonResponse<List<Users>> getAllUsers() {
    return ResponHelper.ok(usersService.getAllUsers());
    }

    @GetMapping("/{Id}")
    public CommonResponse<Users> getUsers(@PathVariable("Id") Integer Id) {
        return ResponHelper.ok(usersService.getUsers(Id));
    }

    @PutMapping(path = "/{Id}", consumes = "multipart/form-data")
    public CommonResponse<Users> editUsersById(@PathVariable("Id") Integer Id, @RequestPart("file") MultipartFile multipartFile, Users users) {
        return ResponHelper.ok(usersService.editUsers(Id,modelMapper.map(users, Users.class),multipartFile));
    }
}
