package com.Presensitodolist.Presensitodolist.controller;

import com.Presensitodolist.Presensitodolist.dto.AbsensiMasukDto;
import com.Presensitodolist.Presensitodolist.model.AbsensiMasuk;
import com.Presensitodolist.Presensitodolist.respon.CommonResponse;
import com.Presensitodolist.Presensitodolist.respon.ResponHelper;
import com.Presensitodolist.Presensitodolist.service.AbsensiMasukService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Absensi_Masuk")
public class AbsensiMasukController {
    // setelah membuat serviceImpl, buat AbsenMasukController
    // dan menerukan dengan membuat model untuk absenPulang

    @Autowired
    private AbsensiMasukService absensiMasukService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all")
    public Object getAll() {
        return ResponHelper.ok(absensiMasukService.getAll());
    }

    @GetMapping("/{Id}")
    public Object getAbsensi(@PathVariable("Id") Integer Id) {
        return ResponHelper.ok(absensiMasukService.getAbsensi(Id));
    }

    @PostMapping
    public CommonResponse<AbsensiMasuk> addAbsensi(@RequestBody AbsensiMasukDto absensiMasukDto){
         return ResponHelper.ok(absensiMasukService.addAbsensi(absensiMasukDto));
    }

    @GetMapping
    public CommonResponse<List<AbsensiMasuk>> getAllByUser(@RequestParam Integer userId) {
        return ResponHelper.ok(absensiMasukService.getAllByUser(userId));
    }

    @PutMapping("/{Id}")
    public CommonResponse<AbsensiMasuk> editAbsensi(@PathVariable("Id") Integer Id, @RequestBody AbsensiMasukDto absensiMasukDto) {
        return ResponHelper.ok(absensiMasukService.editAbsensi(Id,modelMapper.map(absensiMasukDto, AbsensiMasukDto.class)));
    }

    @DeleteMapping("/{Id}")
    public void deleteAbsensi(@PathVariable("Id") Integer Id) {
        absensiMasukService.deleteAbsensi(Id);
    }

}
