package com.Presensitodolist.Presensitodolist.controller;

import com.Presensitodolist.Presensitodolist.dto.AbsensiPulangDto;
import com.Presensitodolist.Presensitodolist.model.AbsensiPulang;
import com.Presensitodolist.Presensitodolist.respon.CommonResponse;
import com.Presensitodolist.Presensitodolist.respon.ResponHelper;
import com.Presensitodolist.Presensitodolist.service.AbsensiPulangService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/Absensi_Pulang")
public class AbsensiPulangController {
    // setelah membuat AbsensiPulangServiceImpl, buat Controller

    @Autowired
    private AbsensiPulangService absensiPulangService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all")
    public Object getAllAbsensi() {
        return ResponHelper.ok(absensiPulangService.getAll());
    }

    @GetMapping("/{Id}")
    public Object getAbsensi(@PathVariable("Id") Integer Id) {
        return ResponHelper.ok(absensiPulangService.getAbsensi(Id));
    }

    @PostMapping
    public CommonResponse<AbsensiPulang> addAbsensi(@RequestBody AbsensiPulangDto absensiPulangDto){
        return ResponHelper.ok(absensiPulangService.addAbsensi(absensiPulangDto));
    }

    @PutMapping("/{Id}")
    public CommonResponse<AbsensiPulang> editAbsensi(@PathVariable("Id") Integer Id, @RequestBody AbsensiPulangDto absensiPulangDto) {
        return ResponHelper.ok(absensiPulangService.editAbsensi(Id, absensiPulangDto));
    }

    @DeleteMapping("/{Id}")
    public void deleteAbsensi(@PathVariable("Id") Integer Id) {
        absensiPulangService.deleteAbsensi(Id);
    }

    @GetMapping
    public CommonResponse<List<AbsensiPulang>> getAllByUser(@RequestParam Integer usersId) {
        return ResponHelper.ok(absensiPulangService.getAllByUser(usersId));
    }
}