package com.Presensitodolist.Presensitodolist.jwt;

import com.Presensitodolist.Presensitodolist.exception.InternalErrorException;
import com.Presensitodolist.Presensitodolist.exception.NotFoundexception;
import com.Presensitodolist.Presensitodolist.model.TemporaryToken;
import com.Presensitodolist.Presensitodolist.model.Users;
import com.Presensitodolist.Presensitodolist.repository.TemporaryTokenRepository;
import com.Presensitodolist.Presensitodolist.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtProvider {
    // setelah membuat JwtAuthTokenFilter baru membuat JwtProvider
    // lalu buat UnautorizeError

    private static String seccetKey = "belajar spring";

    private static Integer expired = 90000;

    @Autowired
    private TemporaryTokenRepository temporaryTokenRepository;

    @Autowired
    private UsersRepository usersRepository;

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("-", "");
//        Users users = usersRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundexception("User not found generate token"));
//        var chekingToken =temporaryTokenRepository.findByUserId(users.getId());
//        if (chekingToken.isPresent()) temporaryTokenRepository.deleteById(chekingToken.get().getId());
//        TemporaryToken temporaryToken = new TemporaryToken();
//        temporaryToken.setToken(token);
//        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
//        temporaryToken.setUserId(users.getId());
//        temporaryTokenRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(()-> new InternalErrorException("token error parse"));
    }

    public boolean chekingTokenJwt(String token) {
        System.out.println(token);
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token Expired");
            return false;
        }
        return true;
    }
}
