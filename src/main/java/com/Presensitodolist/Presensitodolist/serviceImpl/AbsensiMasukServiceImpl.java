package com.Presensitodolist.Presensitodolist.serviceImpl;

import com.Presensitodolist.Presensitodolist.dto.AbsensiMasukDto;
import com.Presensitodolist.Presensitodolist.exception.InternalErrorException;
import com.Presensitodolist.Presensitodolist.exception.NotFoundexception;
import com.Presensitodolist.Presensitodolist.model.AbsensiMasuk;
import com.Presensitodolist.Presensitodolist.repository.AbsensiMasukRepository;
import com.Presensitodolist.Presensitodolist.repository.UsersRepository;
import com.Presensitodolist.Presensitodolist.service.AbsensiMasukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class AbsensiMasukServiceImpl implements AbsensiMasukService {
    // setelah membuat Service, buat ServiceImpl
    // lalu membuat controller

    private static final int HOUR = 3600 * 1000;

    @Autowired
    AbsensiMasukRepository absensiMasukRepository;

    @Autowired
    UsersRepository usersRepository;

    @Transactional
    @Override
    public AbsensiMasuk addAbsensi(AbsensiMasukDto absensiMasukDto) {
        AbsensiMasuk absensiMasuk = new AbsensiMasuk();
        absensiMasuk.setAbsen_masuk(new Date(new Date().getTime() + 3 * HOUR));
        absensiMasuk.setStatus(absensiMasukDto.getStatus());
        absensiMasuk.setUserId(usersRepository.findById(absensiMasukDto.getUserId()).orElseThrow(() -> new NotFoundexception("id tidak ada")));
        return absensiMasukRepository.save(absensiMasuk);
    }

    @Override
    public List<AbsensiMasuk> getAll() {
        return absensiMasukRepository.findAll();
    }

    @Override
    public List<AbsensiMasuk> getAllByUser(Integer userId) {
        return absensiMasukRepository.findUser(userId);
    }

    @Transactional
    @Override
    public AbsensiMasuk getAbsensi(Integer Id){
        var absensi = absensiMasukRepository.findById(Id).orElseThrow(() -> new NotFoundexception("Id tidak ditemukan"));
        try {
            return absensiMasukRepository.save(absensi);
        }catch (Exception e) {
            throw new InternalErrorException("Kesalahan memunculkan data");
        }
    }

    @Transactional
    @Override
    public AbsensiMasuk editAbsensi(Integer Id, AbsensiMasukDto absensiMasukDto){
        AbsensiMasuk update = absensiMasukRepository.findById(Id).orElseThrow(() -> new NotFoundexception("Absensi Id Not Found"));
        update.setStatus(absensiMasukDto.getStatus());
        return absensiMasukRepository.save(update);
    }

    @Transactional
    @Override
    public void deleteAbsensi(Integer Id) {
        absensiMasukRepository.deleteById(Id);
    }
}
