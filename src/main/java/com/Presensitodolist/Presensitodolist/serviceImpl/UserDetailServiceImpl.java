package com.Presensitodolist.Presensitodolist.serviceImpl;

import com.Presensitodolist.Presensitodolist.model.UserPrinciple;
import com.Presensitodolist.Presensitodolist.model.Users;
import com.Presensitodolist.Presensitodolist.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    // membuat userdetailService dahulu sebelum dihubungkan ke class UserServiceImpl
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Object users = usersRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("Username not found"));
        return UserPrinciple.build((Users) users);
    }
}
