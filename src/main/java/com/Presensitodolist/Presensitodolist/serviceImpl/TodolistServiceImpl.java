package com.Presensitodolist.Presensitodolist.serviceImpl;

import com.Presensitodolist.Presensitodolist.dto.TodolistDto;
import com.Presensitodolist.Presensitodolist.exception.NotFoundexception;
import com.Presensitodolist.Presensitodolist.model.Todolist;
import com.Presensitodolist.Presensitodolist.repository.TodolistRepository;
import com.Presensitodolist.Presensitodolist.repository.UsersRepository;
import com.Presensitodolist.Presensitodolist.service.TodolistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TodolistServiceImpl implements TodolistService {
    // setelah buat Service, buat ServiceImpl
    // lalu buat TodolistController

    @Autowired
    TodolistRepository todolistRepository;

    @Autowired
    UsersRepository usersRepository;

    @Override
    public List<Todolist> getAllTodolist(Integer useresId) {
        return todolistRepository.findUser(useresId);
    }

    @Override
    public Todolist addTodolist(TodolistDto todolistDto){
        Todolist todolist1= new Todolist();
        todolist1.setNote(todolistDto.getNote());
        todolist1.setUserId(usersRepository.findById(todolistDto.getUserId()).orElseThrow(() -> new NotFoundexception("userId tidak ditemukan")));
        return todolistRepository.save(todolist1);
    }

    @Override
    public Todolist getTodolist(Integer Id){
        return todolistRepository.findById(Id).orElseThrow(() -> new NotFoundexception("Id tidak ditemukan"));
    }

    @Override
    public Todolist editTodolist(Integer Id, TodolistDto todolistDto){
        Todolist update = todolistRepository.findById(Id).orElseThrow(() -> new NotFoundexception("Id Tidak ditemukan"));
        update.setNote(todolistDto.getNote());
        return todolistRepository.save(update);
    }

    @Override
    public void deleteTodolistById(Integer Id){
        todolistRepository.deleteById(Id);
    }

    @Override
    public List<Todolist> getAllByUser(Integer useresId){
        return todolistRepository.findUser(useresId);
    }

    // untuk membuat cheklis
    static final String done = "✅";
    @Transactional
    @Override
    public Todolist checklist(Integer Id) {
        Todolist update = todolistRepository.findById(Id).orElseThrow(() -> new NotFoundexception("Id Tidak ditemukan"));
        update.setDone(done);
        return update;
    }


}

