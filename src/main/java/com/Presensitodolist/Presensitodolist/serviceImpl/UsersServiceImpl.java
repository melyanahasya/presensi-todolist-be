package com.Presensitodolist.Presensitodolist.serviceImpl;

import com.Presensitodolist.Presensitodolist.dto.LoginDto;
import com.Presensitodolist.Presensitodolist.dto.UsersDto;
import com.Presensitodolist.Presensitodolist.enumed.UserType;
import com.Presensitodolist.Presensitodolist.exception.InternalErrorException;
import com.Presensitodolist.Presensitodolist.exception.NotFoundexception;
import com.Presensitodolist.Presensitodolist.jwt.JwtProvider;
import com.Presensitodolist.Presensitodolist.model.Users;
import com.Presensitodolist.Presensitodolist.repository.UsersRepository;
import com.Presensitodolist.Presensitodolist.service.UsersService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UsersServiceImpl implements UsersService {
    // setelah membuat Service, buat userDetailService dulu
    // lalu buat userServiceImpl dan  setelah ini membuat loginDTO dan controller

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/profil-prestodo.appspot.com/o/%s?alt=media";

    @Autowired
    UsersRepository usersRepository;

     @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    PasswordEncoder passwordEncoder;

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Password Not Found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }


    @Override
    public Map<String, Object> login(LoginDto loginDto) {
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        Object users = usersRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response =  new HashMap<>();
        response.put("token", token);
        response.put("expired", "60 menit");
        response.put("users", users);
        return response;
    }
    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    }


    @Override
    public Users addUsers( UsersDto usersDto) {
        String email = usersDto.getEmail();
        Users users1 = new Users(usersDto.getEmail(),usersDto.getPassword(), usersDto.getNama(),UserType.USER);
        users1.setRole(UserType.USER);
        users1.setPassword(passwordEncoder.encode(usersDto.getPassword()));
        var validasi = usersRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw  new InternalErrorException("Email Alredy Axist");
        }
        return usersRepository.save(users1);
    }


    @Override
    public Users getUsers(Integer Id) {
        return usersRepository.findById(Id).orElseThrow(() -> new NotFoundexception("Id tidak ditemukan")) ;
    }

    @Override
    public void deleteUsersById(Integer Id) {
        usersRepository.deleteById(Id);
    }


    @Transactional
    @Override
    public Users editUsers(Integer Id, Users users, MultipartFile multipartFile){
        String gambar = convertToBase64Url(multipartFile);
        users.setPassword(passwordEncoder.encode(users.getPassword()));
        Users update = usersRepository.findById(Id).orElseThrow(() -> new NotFoundexception("tidak ada"));
        var validasi = usersRepository.findByEmail(users.getEmail());
        if (validasi.isPresent()) {
            throw new InternalErrorException("maaf email sudah ada");
        }
        update.setEmail(users.getEmail());
        update.setPassword(users.getPassword());
        update.setNama(users.getNama());
        update.setTelepon(users.getTelepon());
        update.setGambar(gambar);
        update.setAlamat(users.getAlamat());
        return update;

    }

    private String convertToBase64Url(MultipartFile file) {
        String url = "";
        try{
            byte[] byte1 = Base64.encodeBase64(file.getBytes());
            String result = new String(byte1);
            url = "data:" + file.getContentType() + ";base64," + result;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            return  url;
        }
    }

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile (file, fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error upload file");
        }
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException{
        BlobId blobId = BlobId.of("profil-prestodo.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtenions(String fileName) {
        return fileName.split("\\.")[0];
    }
}
