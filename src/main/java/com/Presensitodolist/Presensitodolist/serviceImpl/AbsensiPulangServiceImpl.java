package com.Presensitodolist.Presensitodolist.serviceImpl;

import com.Presensitodolist.Presensitodolist.dto.AbsensiPulangDto;
import com.Presensitodolist.Presensitodolist.exception.InternalErrorException;
import com.Presensitodolist.Presensitodolist.exception.NotFoundexception;
import com.Presensitodolist.Presensitodolist.model.AbsensiPulang;
import com.Presensitodolist.Presensitodolist.repository.AbsensiPulangRepository;
import com.Presensitodolist.Presensitodolist.repository.UsersRepository;
import com.Presensitodolist.Presensitodolist.service.AbsensiPulangService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AbsensiPulangServiceImpl implements AbsensiPulangService {
    // setelah membuat Service, buat AbsensiPulangServiceImpl
    // lalu buat Controller

    @Autowired
    AbsensiPulangRepository absensiPulangRepository;

    @Autowired
    UsersRepository usersRepository;

    @Transactional
    @Override
    public AbsensiPulang addAbsensi(AbsensiPulangDto absensiPulangDto) {
        AbsensiPulang absensiPulang = new AbsensiPulang();
        absensiPulang.setStatus(absensiPulangDto.getStatus());
        absensiPulang.setUserId(usersRepository.findById(absensiPulangDto.getUserId()).orElseThrow(() -> new NotFoundexception("userId tidak ada")));
    return absensiPulangRepository.save(absensiPulang);
    }

    @Override
    public List<AbsensiPulang> getAll() {
        return absensiPulangRepository.findAll();
    }

    @Transactional
    @Override
    public AbsensiPulang getAbsensi(Integer Id){
        var absensi = absensiPulangRepository.findById(Id).orElseThrow(() -> new NotFoundexception("Id tidak ditemukan"));
        try {
            return absensiPulangRepository.save(absensi);
        }catch (Exception e) {
            throw new InternalErrorException("Kesalahan memunculkan data");
        }
    }

    @Transactional
    @Override
    public AbsensiPulang editAbsensi(Integer Id, AbsensiPulangDto absensiPulangDto){
        AbsensiPulang update = absensiPulangRepository.findById(Id).orElseThrow(() -> new NotFoundexception("Absensi Id Not Found"));
        update.setStatus(absensiPulangDto.getStatus());
        return absensiPulangRepository.save(update);
    }

    @Transactional
    @Override
    public void deleteAbsensi(Integer Id) {
        absensiPulangRepository.deleteById(Id);
    }

    @Override
    public List<AbsensiPulang> getAllByUser(Integer usersId) {
        return absensiPulangRepository.findUser(usersId);
    }
}
