package com.Presensitodolist.Presensitodolist.exception;

public class InternalErrorException extends  RuntimeException{
    // dibuat setelah NotFoundexception
    // lalu membuat GlobalExceptionHandler
    public InternalErrorException(String message) {
        super(message);
    }
}
