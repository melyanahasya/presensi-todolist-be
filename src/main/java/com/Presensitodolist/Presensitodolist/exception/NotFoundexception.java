package com.Presensitodolist.Presensitodolist.exception;

public class NotFoundexception extends RuntimeException{
    // langkah ketiga membuat NotFoundexception
    // setelah itu membuat InternalErrorException
    public NotFoundexception(String message) {
        super(message);
    }
}
