package com.Presensitodolist.Presensitodolist.configuration;

import com.Presensitodolist.Presensitodolist.jwt.Accesdenied;
import com.Presensitodolist.Presensitodolist.jwt.JwtAuthTokenFilter;
import com.Presensitodolist.Presensitodolist.jwt.UnautorizeError;
import com.Presensitodolist.Presensitodolist.serviceImpl.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig  extends WebSecurityConfigurerAdapter {
    // WebSecurityConfig dibuat setelah AppConfig
    @Autowired
    private Accesdenied accessDeniedHandler;

    @Autowired
    private UnautorizeError unautorizeError;

    @Autowired
    private UserDetailServiceImpl userDetailServiceImpl;

    private static final String[] AUTH_WHITLIST = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**",
            "/authentication/**",
            "/users/sign-in",
            "/users/sign-up",
            "/users/**",
            "/todolist/all",
            "/todolist/{Id}",
            "/todolist/done/{Id}",
            "/todolist/**",
            "/Absensi_Masuk/all",
            "/Absensi_Masuk/{Id}",
            "/Absensi_Masuk/**",
            "/Absensi_Pulang/all",
            "/Absensi_Pulang/{Id}",
            "/Absensi_Pulang/**",
            "/gambar",
            "/"
    };

    @Bean
    public JwtAuthTokenFilter authTokenFilter() {
        return new JwtAuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailServiceImpl).passwordEncoder(passwordEncoder() );
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().exceptionHandling().authenticationEntryPoint(unautorizeError).and().exceptionHandling().accessDeniedHandler(accessDeniedHandler).and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests().antMatchers(AUTH_WHITLIST).permitAll().anyRequest().authenticated();
        http.addFilterBefore(authTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }
}