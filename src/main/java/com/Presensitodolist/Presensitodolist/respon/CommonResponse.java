package com.Presensitodolist.Presensitodolist.respon;

public class CommonResponse<T>{
    // langkah kelima membuat CommonResponse dan ResponHelper
    private String message;

    private String status;

    private T data;

    public CommonResponse() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
